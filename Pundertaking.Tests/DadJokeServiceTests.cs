using Pundertaking.Services;
using Xunit;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;
using Pundertaking.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Pundertaking.Tests
{
    public class DadJokeServiceTests
    {
        private readonly DadJokeService _sut;
        private readonly HttpClient _client;
        private readonly IConfiguration _config;
        private readonly ILogger<IDadJokeService> _logger;

        public DadJokeServiceTests()
        {
            _client = new HttpClient(){
                BaseAddress = new Uri("https://icanhazdadjoke.com/")
            };
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            _client.DefaultRequestHeaders.TryAddWithoutValidation(
               "User-Agent",
               "Pundertaking (Pundertaking Test suite)"
            );

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            _config = builder.Build();

            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();
            var factory = serviceProvider.GetService<ILoggerFactory>();
            _logger = factory.CreateLogger<IDadJokeService>();

            _sut = new DadJokeService(_client, _config, _logger);
        }

        #region Tests With Passing Data:

        [Fact]
        public async void GetRandomJoke_ShouldReturnJoke()
        {
            var viewModel = await _sut.GetRandomJoke();

            Assert.NotNull(viewModel);
            Assert.True(viewModel.Jokes.Length > 0);
        }

        [Theory]
        [InlineData("one", 30)]
        [InlineData("one", 20)]
        [InlineData("one", 10)]
        public async void SearchJokes_ShouldReturnEqualOrLessNumOfJokes(string terms, int expectedNumOfJokes)
        {
            var viewModel = await _sut.SearchJokes(terms, expectedNumOfJokes);

            Assert.NotNull(viewModel);

            //remove delimiter entires
            viewModel.Jokes = viewModel.Jokes.Where(j => j != "~~~").ToArray();

            Assert.True(viewModel.Jokes.Length <= expectedNumOfJokes);
        }

        [Theory]
        [MemberData(nameof(SearchJokes_PassingTestData))]
        public async void SearchJokes_ShouldEmphasizeSearchTerm(
            JokeViewModel testDataVM,
            params object[] _
        )
        {
            if (string.IsNullOrEmpty(testDataVM.Terms))
            {
                return;
            }

            var viewModel = await _sut.SearchJokes(testDataVM.Terms);

            for (int j = 0; j < viewModel.Jokes.Length; j++)
            {
                if (viewModel.Jokes[j].Equals("~~~"))
                {
                    continue;
                }
                Assert.Contains(
                    "<strong>" + testDataVM.Terms + "</strong>",
                    viewModel.Jokes[j],
                    StringComparison.CurrentCultureIgnoreCase
                );
            }
        }

        [Theory]
        [MemberData(nameof(SearchJokes_PassingTestData))]
        public async void SearchJokes_ShouldGroupJokes(
            JokeViewModel testDataVM,
            int numSmallJokes,
            int numMedJokes,
            int numLargeJokes
        )
        {
            var viewModel = await _sut.SearchJokes(testDataVM.Terms);

            int groupCount = 1;
            int smJokes = 0;
            int mdJokes = 0;
            int lgJokes = 0;
            string[] words;

            for (int j = 0; j < viewModel.Jokes.Length; j++)
            {
                if (viewModel.Jokes[j].Equals("~~~"))
                {
                    groupCount++;
                    continue;
                }

                words = viewModel.Jokes[j].Split(null);
                if (groupCount == 1)
                {
                    smJokes++;
                    Assert.True(words.Length <= _config.GetValue("MaxNumWordShort", 9));
                }
                else if (groupCount == 2)
                {
                    mdJokes++;
                    Assert.True(words.Length <= _config.GetValue("MaxNumWordMed", 19));
                }
                else
                {
                    lgJokes++;
                    Assert.True(words.Length > _config.GetValue("MaxNumWordMed", 19));
                }
            }

            if(viewModel.Jokes.Length > 0){
                Assert.Equal(3, groupCount);
            }

            if(!string.IsNullOrWhiteSpace(testDataVM.Terms)){
                Assert.Equal(numSmallJokes, smJokes);
                Assert.Equal(numMedJokes, mdJokes);
                Assert.Equal(numLargeJokes, lgJokes);
            }
        }

        #endregion

        #region Tests With Failing Data

        #endregion

        #region Test Data

        public static IEnumerable<object[]> SearchJokes_PassingTestData()
        {
            yield return new object[] {
                new JokeViewModel() {
                    Terms = "dog",
                    Jokes = new string[] {
                        "Why did the cowboy have a weiner dog? Somebody told him to get a long little doggy.",
                        "Me: If humans lose the ability to hear high frequency volumes as they get older, can my 4 week old son hear a dog whistle?  Doctor: No, humans can never hear that high of a frequency no matter what age they are.  Me: Trick question... dogs can&#39;t whistle.",
                        "I went to the zoo the other day, there was only one dog in it. It was a shitzu.",
                        "what do you call a dog that can do magic tricks? a labracadabrador\"",
                        "�My Dog has no nose.� �How does he smell?� �Awful�",
                        "What kind of dog lives in a particle accelerator? A Fermilabrador Retriever.",
                        "My dog used to chase people on a bike a lot. It got so bad I had to take his bike away.",
                        "I adopted my dog from a blacksmith. As soon as we got home he made a bolt for the door.",
                        "What did the dog say to the two trees? Bark bark.",
                        "At the boxing match, the dad got into the popcorn line and the line for hot dogs, but he wanted to stay out of the punchline.",
                        "It was raining cats and dogs the other day. I almost stepped in a poodle.",
                        "What did the Zen Buddist say to the hotdog vendor? Make me one with everything."
                    }
                },
                0,
                8,
                4
            };
            yield return new object[] {
                new JokeViewModel() {
                    Terms = "one",
                    Jokes = new string[] {
                        "What was a more important invention than the first telephone? The second one.",
                        "A police officer caught two kids playing with a firework and a car battery. He charged one and let the other one off.",
                        "I really want to buy one of those supermarket checkout dividers, but the cashier keeps putting it back.",
                        "What did the Zen Buddist say to the hotdog vendor? Make me one with everything.",
                        "What&#39;s the difference between a hippo and a zippo? One is really heavy, the other is a little lighter.",
                        "Two peanuts were walking down the street. One was a salted",
                        "Last night me and my girlfriend watched three DVDs back to back. Luckily I was the one facing the TV.",
                        "Did you hear the one about the guy with the broken hearing aid? Neither did he.",
                        "Two parrots are sitting on a perch. One turns to the other and asks, &quot;do you smell fish?&quot;",
                        "How do you tell the difference between a crocodile and an alligator? You will see one later and one in a while.",
                        "Did you hear the one about the giant pickle?  He was kind of a big dill.",
                        "This furniture store keeps emailing me, all I wanted was one night stand!",
                        "Two peanuts were walking down the street. One was a salted.",
                        "Two fish are in a tank, one turns to the other and says, &quot;how do you drive this thing?&quot;",
                        "Did you know you should always take an extra pair of pants golfing? Just in case you get a hole in one.",
                        "What did one snowman say to the other snow man? Do you smell carrot?",
                        "Some people say that comedians who tell one too many light bulb jokes soon burn out, but they don&#39;t know watt they are talking about. They&#39;re not that bright.",
                        "Why is no one friends with Dracula? Because he&#39;s a pain in the neck.",
                        "I went to the zoo the other day, there was only one dog in it. It was a shitzu.",
                        "What did one wall say to the other wall? I&#39;ll meet you at the corner!",
                        "What did one plate say to the other plate? Dinner is on me!",
                        "A man walked in to a bar with some asphalt on his arm. He said �Two beers please, one for me and one for the road.�",
                        "What did one nut say as he chased another nut?  I&#39;m a cashew!",
                        "Chances are if you&#39; ve seen one shopping center, you&#39;ve seen a mall.",
                        "I gave my friend 10 puns hoping that one of them would make him laugh. Sadly, no pun in ten did.",
                        "Two guys walked into a bar, the third one ducked.",
                        "Want to hear a chimney joke? Got stacks of em! First one&#39;s on the house",
                        "Never Trust Someone With Graph Paper...  They&#39;re always plotting something.",
                        "Where do you take someone who has been injured in a Peek-a-boo accident? To the I.C.U.",
                        "Don�t interrupt someone working intently on a puzzle. Chances are, you�ll hear some crosswords."
                    }
                },
                0,
                23,
                7
            };
            yield return new object[] {
                new JokeViewModel() {
                    Terms = "",
                    Jokes = new string[] {
                        "What was a more important invention than the first telephone? The second one.",
                        "A police officer caught two kids playing with a firework and a car battery. He charged one and let the other one off.",
                        "I really want to buy one of those supermarket checkout dividers, but the cashier keeps putting it back.",
                        "What did the Zen Buddist say to the hotdog vendor? Make me one with everything.",
                        "What&#39;s the difference between a hippo and a zippo? One is really heavy, the other is a little lighter.",
                        "Two peanuts were walking down the street. One was a salted",
                        "Last night me and my girlfriend watched three DVDs back to back. Luckily I was the one facing the TV.",
                        "Did you hear the one about the guy with the broken hearing aid? Neither did he.",
                        "Two parrots are sitting on a perch. One turns to the other and asks, &quot;do you smell fish?&quot;",
                        "How do you tell the difference between a crocodile and an alligator? You will see one later and one in a while.",
                        "Did you hear the one about the giant pickle?  He was kind of a big dill.",
                        "This furniture store keeps emailing me, all I wanted was one night stand!",
                        "Two peanuts were walking down the street. One was a salted.",
                        "Two fish are in a tank, one turns to the other and says, &quot;how do you drive this thing?&quot;",
                        "Did you know you should always take an extra pair of pants golfing? Just in case you get a hole in one.",
                        "What did one snowman say to the other snow man? Do you smell carrot?",
                        "Some people say that comedians who tell one too many light bulb jokes soon burn out, but they don&#39;t know watt they are talking about. They&#39;re not that bright.",
                        "Why is no one friends with Dracula? Because he&#39;s a pain in the neck.",
                        "I went to the zoo the other day, there was only one dog in it. It was a shitzu.",
                        "What did one wall say to the other wall? I&#39;ll meet you at the corner!",
                        "What did one plate say to the other plate? Dinner is on me!",
                        "A man walked in to a bar with some asphalt on his arm. He said �Two beers please, one for me and one for the road.�",
                        "What did one nut say as he chased another nut?  I&#39;m a cashew!",
                        "Chances are if you&#39; ve seen one shopping center, you&#39;ve seen a mall.",
                        "I gave my friend 10 puns hoping that one of them would make him laugh. Sadly, no pun in ten did.",
                        "Two guys walked into a bar, the third one ducked.",
                        "Want to hear a chimney joke? Got stacks of em! First one&#39;s on the house",
                        "Never Trust Someone With Graph Paper...  They&#39;re always plotting something.",
                        "Where do you take someone who has been injured in a Peek-a-boo accident? To the I.C.U.",
                        "Don�t interrupt someone working intently on a puzzle. Chances are, you�ll hear some crosswords."
                    }
                },
                0,
                23,
                7
            };
            yield return new object[] {
                new JokeViewModel() {
                    Terms = "atudnfwerkcs",
                    Jokes = new string[0]
                },
                0,
                0,
                0
            };
            yield return new object[] {
                new JokeViewModel() {
                    Terms = "",
                    Jokes = new string[] {
                        "I went to the zoo the other day, there was only one dog in it. It was a shitzu.",
                        "What was a more important invention than the first telephone? The second one.",
                        "Two peanuts were walking down the street. One was a salted.",
                        "Two fish are in a tank, one turns to the other and says, &quot;how do you drive this thing?&quot;",
                        "Why is no one friends with Dracula? Because he&#39;s a pain in the neck.",
                        "What did the Zen Buddist say to the hotdog vendor? Make me one with everything.",
                        "Did you hear the one about the guy with the broken hearing aid? Neither did he.",
                        "What did one snowman say to the other snow man? Do you smell carrot?",
                        "What did one wall say to the other wall? I&#39;ll meet you at the corner!",
                        "Two guys walked into a bar, the third one ducked.",
                        "I really want to buy one of those supermarket checkout dividers, but the cashier keeps putting it back.",
                        "What&#39;s the difference between a hippo and a zippo? One is really heavy, the other is a little lighter.",
                        "Two parrots are sitting on a perch. One turns to the other and asks, &quot;do you smell fish?&quot;",
                        "Did you hear the one about the giant pickle?  He was kind of a big dill.",
                        "What did one plate say to the other plate? Dinner is on me!",
                        "Two peanuts were walking down the street. One was a salted",
                        "Want to hear a chimney joke? Got stacks of em! First one&#39;s on the house",
                        "Where do you take someone who has been injured in a Peek-a-boo accident? To the I.C.U.",
                        "I�ve deleted the phone numbers of all the Germans I know from my mobile phone. Now it�s Hans free.",
                        "Why was the robot angry? Because someone kept pressing his buttons!",
                        "As I suspected, someone has been adding soil to my garden. The plot thickens.",
                        "How do you tell the difference between a crocodile and an alligator? You will see one later and one in a while.",
                        "A man walked in to a bar with some asphalt on his arm. He said �Two beers please, one for me and one for the road.�",
                        "A police officer caught two kids playing with a firework and a car battery. He charged one and let the other one off.",
                        "Last night me and my girlfriend watched three DVDs back to back. Luckily I was the one facing the TV.",
                        "Some people say that comedians who tell one too many light bulb jokes soon burn out, but they don&#39;t know watt they are talking about. They&#39;re not that bright.",
                        "I gave my friend 10 puns hoping that one of them would make him laugh. Sadly, no pun in ten did.",
                        "If you�re struggling to think of what to get someone for Christmas. Get them a fridge and watch their face light up when they open it.",
                        "Did you hear the news? FedEx and UPS are merging. They�re going to go by the name Fed-Up from now on.",
                        "The Swiss must&#39;ve been pretty confident in their chances of victory if they included a corkscrew in their army knife."
                        }
                },
                0,
                21,
                9
            };
        }

        #endregion
    }
}
