﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Pundertaking.Models;
using Pundertaking.Services;

namespace Pundertaking.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IDadJokeService _dadJokeService;

        public HomeController(
            ILogger<HomeController> logger,
            IDadJokeService dadJokeService
        )
        {
            _logger = logger;
            _dadJokeService = dadJokeService;
        }

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// User endpoint wrapper for <see cref="IDadJokeService.GetRandomJoke"/>
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> GetRandomJoke()
        {
            return Json(await _dadJokeService.GetRandomJoke());
        }

        /// <summary>
        /// User endpoint wrapper for <see cref="IDadJokeService.SearchJokes"/>
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> SearchJokes(string terms)
        {
            return Json(await _dadJokeService.SearchJokes(terms));
        }
    }
}
