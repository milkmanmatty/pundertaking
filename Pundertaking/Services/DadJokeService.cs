﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Configuration;
using Pundertaking.Models;
using Microsoft.Extensions.Logging;

namespace Pundertaking.Services
{
    public class DadJokeService : IDadJokeService
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _config;
        private readonly ILogger<IDadJokeService> _logger;

        public DadJokeService(
            HttpClient client,
            IConfiguration config,
            ILogger<IDadJokeService> logger
        )
        {
            _client = client;
            _config = config;
            _logger = logger;
        }

        public async Task<JokeViewModel> GetRandomJoke()
        {
            var result = new JokeViewModel();
            try
            {
                result.Terms = null;
                result.Jokes = new string[] {
                    //Attempt to curb XSS
                    HttpUtility.HtmlEncode(
                        (await _client.GetStringAsync("/")).Replace("\r\n", " ")
                    )
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        public async Task<JokeViewModel> SearchJokes(string term, int limit = 30)
        {
            var result = new JokeViewModel();
            try
            {
                //Clamp to acceptable values
                limit = Math.Min(limit, 30);
                limit = Math.Max(limit, 1);

                var query = HttpUtility.ParseQueryString(string.Empty);
                query["page"] = "1";
                query["limit"] = "" + limit;
                query["term"] = term;
                string queryString = query.ToString();

                var matchingBlob = await _client.GetStringAsync("/search?" + queryString);

                if (matchingBlob != null && matchingBlob.Length > 0)
                {
                    var results = matchingBlob.Replace("\r\n", " ").Split("\n", StringSplitOptions.RemoveEmptyEntries);

                    //Trim and HTMLencode data from third-party source.
                    for (int r = 0; r < results.Length; r++)
                    {
                        results[r] = HttpUtility.HtmlEncode(
                            results[r].Trim()
                        );
                    }

                    var terms = term.Split(null);
                    results = GroupAndEmphasizeJokes(results, terms);

                    return new JokeViewModel()
                    {
                        Terms = null,
                        Jokes = results
                    };
                }

                result.Terms = term;
                result.Jokes = new string[0];
                result.ErrorMessage = "";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                result.ErrorMessage = ex.Message;
            }

            return result;
        }

        /// <param name="terms">Optional, the System.String array containing the terms to be emphasized.</param>
        /// <param name="joke">The System.String to have parts be emphasized.</param>
        /// <returns>A System.String with all occurances of <paramref name="terms"/> within <paramref name="joke"/> wrapped with: <c>&lt;strong>&lt;/strong></c>. If <paramref name="terms"/> is null or empty then <paramref name="joke"/> is returned unaltered.</returns>
        private string EmphasizeJokeTerm(string[] terms, string joke)
        {
            if (terms == null || terms.Length == 0)
            {
                return joke;
            }

            //The plural of Regex is Regrets - little dad joke for those reading.
            return Regex.Replace(
                joke,
                string.Join("|", terms),
                "<strong>$&</strong>",
                RegexOptions.IgnoreCase
            );
        }

        /// <summary>
        /// Sorts the <paramref name="jokes"/> string array into 3 groups based on word count with <paramref name="groupDelimiter"/> as the group seperators. Jokes will attempt to be emphasized by <see cref="EmphasizeJokeTerm"/>.
        /// </summary>
        /// <param name="jokes">The System.String array of jokes to sort.</param>
        /// <param name="termToEmphasize">The System.String array to pass to EmphasizeJokeTerm for emphasis.</param>
        /// <param name="groupDelimiter">The System.String instance to pass to EmphasizeJokeTerm for emphasis.</param>
        /// <returns>A System.String array containing exactly 3 groups of jokes sorted by word count and separated by <paramref name="groupDelimiter"/>. If provided, the jokes will have substrings matching any <paramref name="termsToEmphasize"/> emphasized.</returns>
        /// <seealso cref="EmphasizeJokeTerm"/>
        private string[] GroupAndEmphasizeJokes(
            string[] jokes,
            string[] termToEmphasize = null,
            string groupDelimiter = "~~~"
        )
        {
            if (string.IsNullOrWhiteSpace(groupDelimiter))
            {
                groupDelimiter = "~~~";
            }

            var sm = new List<string>();
            var md = new List<string>();
            var lg = new List<string>();

            for (int j = 0; j < jokes.Length; j++)
            {
                var words = NumWordsInJoke(jokes[j]);
                if (words <= _config.GetValue("MaxNumWordShort", 9))
                {
                    sm.Add(
                        EmphasizeJokeTerm(termToEmphasize, jokes[j])
                    );
                }
                else if (words <= _config.GetValue("MaxNumWordMed", 19))
                {
                    md.Add(
                        EmphasizeJokeTerm(termToEmphasize, jokes[j])
                    );
                }
                else //if (words > _config.GetValue("MaxNumWordMed", 19))
                {
                    lg.Add(
                        EmphasizeJokeTerm(termToEmphasize, jokes[j])
                    );
                }
            }

            sm.Add(groupDelimiter);
            sm.AddRange(md);
            sm.Add(groupDelimiter);
            sm.AddRange(lg);

            return sm.ToArray();
        }

        private int NumWordsInJoke(string joke)
        {
            return joke.Split(null).Length;
        }
    }
}
