﻿using System.Threading.Tasks;
using Pundertaking.Models;

namespace Pundertaking.Services
{
    public interface IDadJokeService
    {
        /// <summary>
        /// Makes a request to the API for a single random joke of indeterminable length.
        /// </summary>
        /// <returns>A populated JokeViewModel containing a single dad joke as returned from the API. </returns>
        public Task<JokeViewModel> GetRandomJoke();

        /// <summary>
        /// Makes a request to the API for <paramref name="limit"/> number of jokes containing the specified <paramref name="term"/>. Jokes returned are of indeterminable length.
        /// </summary>
        /// <param name="term">A System.String representing the terms that must be present in each joke returned by the API.</param>
        /// <param name="limit">The maximum number of jokes to be returned. Acceptable value range is 1-30, values outside of this range will be clamped.</param>
        /// <returns>A populated JokeViewModel containing jokes matching the search parameters. Actual <c>JokeViewModel.Jokes</c> length will be <paramref name="limit"/> plus 2 delimiter elements. If no jokes match the search parameters an empty array is populated into <c>JokeViewModel.Jokes</c>.</returns>
        public Task<JokeViewModel> SearchJokes(string term, int limit = 30);
    }
}
