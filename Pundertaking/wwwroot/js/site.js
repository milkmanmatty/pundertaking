﻿(function () {
    document.querySelector(".rnd").addEventListener("click", async function () {
        appendMessage(false, "I'm feeling random");
        let newjoke = await (await fetch("/Home/GetRandomJoke/")).json();
        appendMessage(true, newjoke.jokes[0]);
    });

    document.querySelector(".srch").addEventListener("click", async function () {
        let terms = document.querySelector("#search_jokes").value;

        if (terms) {
            appendMessage(false, generateRandomPreface() + '"' + terms + '" jokes');
        } else {
            appendMessage(false, generateRandomPreface() + 'jokes');
        }
        
        let foundJokes = await (await fetch("/Home/SearchJokes/" + terms)).json();

        if (foundJokes.jokes.length === 0) {
            appendMessage(true, "No jokes on that :(");
            return;
        } else if (foundJokes.ErrorMessage) {
            appendMessage(true, "No jokes on that :(");
            return;
        }

        let group = 0;
        appendMessage(true, "Short jokes:");

        for (let i = 0; i < foundJokes.jokes.length; i++) {
            if (foundJokes.jokes[i] === "~~~") {
                group++;
                switch (group) {
                    case 1: appendMessage(true, "Medium jokes:"); break;
                    case 2: appendMessage(true, "Long jokes:"); break;
                    default: break;
                }
            } else {
                appendMessage(true, foundJokes.jokes[i]);
            }
        }
    });

    function appendMessage(left, msgText, error = false) {
        let row = document.createElement('div');
        row.classList.add('row');
        row.classList.add(left ? 'dad' : 'user');
        if (error) {
            row.classList.add('error');
        }

        let message = document.createElement('p');
        message.innerHTML = msgText; //our endpoint encodes all entites already

        row.appendChild(message);
        document.querySelector('.messages').appendChild(row);
    }

    const prefaces = [
        "I want some ",
        "I'd like to heard about the ",
        "Hit me with those ",
        "I'm ready for some ",
    ];
    function generateRandomPreface() {
        let p = Math.floor(Math.random() * prefaces.length);
        return prefaces[p];
    }
})()