# Pundertaking
=====

Pundertaking is an example implementation of a coding challenge supplied with the following requirements:

Create an application that uses the "I can haz dad joke" API (https://icanhazdadjoke.com/api) to display jokes.
 
The front-end code should be as simple as possible. Use any front-end mechanism you wish but keep it simple. We're interested in how you would design and implement the back-end service.
 
As such, all the communication with the icanhazdadjoke API and preparing the data for display should be implemented in a back-end service using C# and .NET.
 
There should be two options the user can choose from:

1. Fetch a random joke.
2. Accept a search term and display the first 30 jokes containing that term. The matching term should be emphasized in some simple way (upper, quotes, angle brackets, etc.). The matching jokes should be grouped by length: Short (<10 words), Medium (<20 words), Long (>= 20 words)


## Live Example
----------------

A live example can be found at <https://pundertaking.milkmanmatty.com> or <https://pundertaking.milkmanmatty.com.au>


## How to Use Pundertaking
----------------

Clone the repo locally, open the solution in Visual Studio and click Run (or press f5).

When running locally the browser will likely throw a TLS certificate warning as the site is set up to use HTTPS. 


## Rationale
----------------

MVC was used because the front-end is given "for free". MVC is very quick to get both the back and front ends working together with correct plumbing and routing. Could easily have used a Web API and a detached front-end instead but the time required for an end product would likely be higher.

Controllers contain no business logic to promote easier testing and better reusability.

The service business logic communicates with the icanhazdadjoke API via plain/text purely because it felt like the more difficult mime-type to use.


## Future Improvements
-------------------

Back-end improvements:

* Add a ILogger implementation that logs to file (such as <https://github.com/nreco/logging>)

Front-end improvements:

* Add a visual cue that "dad" is typing a message after the user clicks a button on the front-end but before the network request completes. After the network request completes the visual cue should be removed.
* Pressing enter submits the search text field (WCAG violation).
* Test accessibility of website via text-only, keyboard only, mouse-only. Styling will likely also change (outlines, colours etc.)
* Limit height to be viewport size, automatically scroll the message view to the last message sent by "dad" after network request returns.
